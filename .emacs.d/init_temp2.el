;;           .      .
;;           ..    ...
;;          ....  .. .                        ...
;;         ..  . ..  .                       ....
;;         .   ...   ..                      . ..
;;       ...    .     .                     ..  ..
;;      .. ++  ++     .                     .    ..
;;      .  ++  ++     ..                    ..    ....
;;     ..       ..     ..                    .       ...
;;    ...........     . ..                   .         ..
;;                 ....  .                  ..  ..    ....
;;                ..     ..                 .... .  ...  .
;;               ..   ... ..               ...   ....    ..
;;              ..  . .    ..              .              .
;;                 ..        ..         ...               .
;;                            ..       ..                ..
;;                   ........ ....   ...               ..
;;                 ...      ........... .           ....
;;                      ....             .......... .
;;                    ...

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)
(use-package use-package
  :custom
  (use-package-enable-imenu-support t)
  ;; (use-package-compute-statistics t)
  ;; (use-package-verbose 'debug)
  )
;;; todo
;; jak keywordy dobrze tabować
;; wyłączyć auto save w main.py klawiatury
;; ulepszyć isearcha xaha w ten sposób, że jak point jest przed markiem, to jest iseach w seleckji, a jak point jest po marku, to yankuje do isearcha
;; coś dziwnie kmk działą z ctrl + F3, wszysktie te sticky źle działają, tak samo z ctrl + '
;; lean4 tutaj i popatrzeć jak ktoś streamuje vs code żebym mógł zobaczyć jak interkatywnie wszystko robić
;; wydaje mi się, że multiple cursors są lepsze, bo wymagają mniej przycisków i są mniej podatne na side effecty i są bardziej intuicyjne, ale makra są prostsze pod tym względem, że łatwiej je naprawić elispem, to jest dobry projekt, zrobić proste makra.
;; keylogger na klawiaturę? Albo ta funkcja, że ma się z góry ustalony tekst i każde kliknięcie daje z listy jedną literkę xdd
;; meow czemu , . nie działa w read only files
;; zrobienie funkcji to formatowania klawy w miejscu. Czy w sumie parsery nie są bardzo powiązane z edytorami?
;; zrobienie vim hardway dla meow, powinno to być łatwe
;; zulip w emacsie
;; funkcja w pythone/innych która zaznacza funkcję i odpala na niej debugger
;; w meow replace % dodać tak, żeby zwykłe zaznaczenie też dodawało automatycznie, nie tylko przy S
;; eldoc-box hook to eldoc, tylko jak poproszę eldoc box eglot at point
;; grammarly w emacsie
;; Jak odzyskać marker position
;; Czemu kursor się przesuwa na linuxie, ale nie na windowsie
;; Czy transpose jest warty nauki?
;; jest pełno fajnych keybindingów pod stare rzeczy typu M-i to tab-to-tab-stop
;; dodać komendę na dodanie linijki na dole i na górze
;; pdf mode
;; jak zrobić dabbrev i podobne z corfu cape-dabbrev
;; yasnippet lub hippie
;; Projekt: pokazanie za pomocą koloru gdzie są obecnie marki, tak dla treningu
;; może hook na after-save delete trailing whitespace?
;; może coś zamiast jupyterlab, typu org coś.
;; tab-line-mode oraz tab-bar-mode https://systemcrafters.net/emacs-tips/tab-bar-workspaces/
;; wykorzystać lepiej moc mojej klawiatury, czyli cały layer ze strzałkami, ale to po kmk. Na mojej standardowej klawiaturze usunąć , oraz . z symboli, bardzo rzadko używam i dalej nie umiem korzystać. Dodatkowo dodać M-n i M-p, bo to jest op w minibufferze
;; w meow dodać poruszanie się za pomoc consult marka i po prostu marka
;; zanim będę ulepszał meow, najpierw nauczę się normalnego emacsa, może emacs manual prostszy i mastering emacs, reszta paczek co mam to dorobić, szczególnie obmyślić jak rozwiązać problem różnych keybindingów z smartparens i consulta. Dodatkowo popatrzeć, które komendy w meow korzystam a które nie, bo chyba z 10 teraz zapisanych nie korzystam
;; meow komenda na kopiuj wklej pod i skomentuj
;; W meow douczyć się beacona, to jest fajniejszy apply-macro-to-region-lines, bo może zacząć makro z innych miejsc niż początek linijki
;; sp-rewrap-sexp i reszta smartparens
;; emacs manual (rectangle commands, calc, Continuation Lines, Expressions, Yanking with apending kills, fill komendy są op, indentation, xref, (desktop-save-mode 1))
;; ustawić do końca minad stack, skorzystać z emacs bedrock
;; avy jest super
;; można f1 f2 zmienić bez problemu
;; gptel
;; emms
;; clojure full setup
;; projectile
;; geiser
;; combobulate
;; embark lepszy
;; elfeed
;; org agenda itp może kiedyś
;; AUCTeX z pdf tools i Tex mode
;; mu4e, ale to jest podobno straszne
;; elpaca
;; customize okienko ogarnąć
;; ement
;; mpc
;; elfeed

;;; xah
(defun xah-search-current-word ()
  "Call `isearch' on current word or selection.
“word” here is A to Z, a to z, and hyphen [-] and lowline [_], independent of syntax table.

URL `http://xahlee.info/emacs/emacs/modernization_isearch.html'
Version: 2015-04-09"
  (interactive)
  (let (xp1 xp2)
    (if (region-active-p)
        (setq xp1 (region-beginning) xp2 (region-end))
      (save-excursion
        (skip-chars-backward "-_A-Za-z0-9")
        (setq xp1 (point))
        (right-char)
        (skip-chars-forward "-_A-Za-z0-9")
        (setq xp2 (point))))
    (setq mark-active nil)
    (when (< xp1 (point))
      (goto-char xp1))
    (isearch-mode t)
    (isearch-yank-string (buffer-substring-no-properties xp1 xp2))))
(keymap-global-set "<f1>" 'xah-search-current-word)


;;; visible mark
(defface visible-mark-active ;; put this before (require 'visible-mark)
  '((((type tty) (class mono)))
    (t (:background "magenta"))) "")
(require 'visible-mark)
(global-visible-mark-mode 1) ;; or add (visible-mark-mode) to specific hooks
(setq visible-mark-max 2)
(setq visible-mark-faces `(visible-mark-face1 visible-mark-face2))
;; (setq transient-mark-mode nil)

;;; lean4
(add-to-list 'load-path "~/github/nael")
(require 'nael)

(defun my-nael-setup ()
  (interactive)
  ;; Enable Emacs' built-in `TeX' input-method.  Alternatively, you
  ;; could install the external `unicode-math-input' package and
  ;; use the `unicode-math' input-method.
  (set-input-method "TeX")
  ;; Enable Emacs' built-in LSP-client Eglot.
  (eglot-ensure))

(add-hook 'nael-mode-hook #'my-nael-setup)

;; Nael buffer-locally sets `compile-command' to "lake build".
(keymap-set nael-mode-map "C-c C-c" #'project-compile)

;; Find out how to type the character at point in the current
;; input-method.
(keymap-set nael-mode-map "C-c C-k" #'quail-show-key)

;;; multiple-cursors
;; (use-package multiple-cursors
;;   :ensure t
;;   :bind (:map global-map
;;               ("M-?" . #'mc/mark-next-like-this) na klawiaturze mojej nie działa, a na laptopowej działa, to chyba przez kmk
;;               ("M-(" . #'mc/mark-previous-like-this)
;;               ("C-c C-<" . #'mc/mark-all-like-this)))

;;; flymake
(use-package flymake
  :ensure nil
  :bind (:map global-map
              ("M-n" . #'flymake-goto-next-error)
              ("M-p" . #'flymake-goto-prev-error)))

;;; ement
(use-package ement
  :ensure nil
  :defer t)

;;; tab-bar, czy C-tab nie jest zbyt fajny keybindingiem? Zastanowić się przy nowej klawiaturze TODO ogranąć tab-bar customize-other-window, chyba tam jest dużo ciekawych opcji
(use-package tab-bar
  :ensure nil
  :config
  (tab-bar-mode t)
  (tab-bar-history-mode t)
  (defvar-keymap tab-bar-history-repeat-map
    :repeat t
    "<left>" #'tab-bar-history-back
    "<right>" #'tab-bar-history-forward)
  :custom
  (tab-bar-show nil)
  :bind (:map tab-prefix-map
              ("C-r" . tab-rename))
  )

(use-package repeat
  :ensure nil
  :config
  (repeat-mode t))

;;; pdf-tools
(use-package pdf-tools
  :ensure t
  :defer t)

;;; clojure
;; (use-package cider
;;   :ensure t
;;   :defer t)

;; (use-package inf-clojure
;;   :ensure t)

;;; ace-window
(use-package ace-window
  :ensure t
  :bind ("M-o" . ace-window))

;;; yasnippet
(use-package yasnippet
  :ensure t
  ;;   :hook (prog-mode . yas-minor-mode)
  :config
  (yas-global-mode t))

(use-package yasnippet-snippets
  :ensure t
  :after yasnippet)

;; File mode specification error: (error Autoloading file /home/esnos/.emacs.d/elpa/sweeprolog-0.27.6/sweeprolog.elc failed to define function sweeprolog) ; coś nie działa
;;; prolog
(use-package sweeprolog
  :ensure t
  ;; :defer t
  ;; :mode ("\\.prolog\\'" . sweeprolog-mode)
  ;; :config
  ;; (add-to-list 'auto-mode-alist '("\\.prolog\\'"  . sweeprolog-mode))
  )

;;; isearch
(use-package isearch
  :ensure nil
  :custom
  (isearch-allow-motion t)
  :bind (:map isearch-mode-map
              ("<up>" . 'isearch-ring-retreat)
              ("<down>" . 'isearch-ring-advance)
              ("<left>" . 'isearch-repeat-backward)
              ("<right>" . 'isearch-repeat-forward)
              :map minibuffer-local-isearch-map
              ("<left>" . 'isearch-reverse-exit-minibuffer)
              ("<right>" . 'isearch-forward-exit-minibuffer)))

;;; erc czemu defer nie działa
(use-package erc
  :ensure nil
  :defer t
  :custom
  (erc-channel-hide-list '(("#emacs" "JOIN" "PART" "QUIT")))
  (erc-hide-list '("JOIN" "PART" "QUIT"))
  (erc-modules
   '(autojoin button completion fill irccontrols list log match menu
              move-to-prompt netsplit networks noncommands readonly
              ring stamp track))
  '(erc-network-hide-list '(("Libera.Chat" "JOIN" "PART" "QUIT"))))

;;; go-translate
(use-package go-translate
  :ensure t
  :custom
  (gt-langs '(en pl))
  (gt-default-translator (gt-translator :engines (gt-google-engine))))

;;; gptel
(use-package gptel
  :ensure t
  :defer t
  :config
  ;; :key can be a function that returns the API key.
  (gptel-make-gemini "Gemini" :key "My-key" :stream t)
  :custom ; dodać funckję na zapisywanie tego w innym tajnym pliku
  (gptel-api-key ""))

;;; markdown mode
(use-package markdown-ts-mode
  :ensure t
  :mode ("\\.md\\'" . markdown-ts-mode)
  :defer t
  :config
  (add-to-list 'treesit-language-source-alist '(markdown "https://github.com/tree-sitter-grammars/tree-sitter-markdown" "split_parser" "tree-sitter-markdown/src"))
  (add-to-list 'treesit-language-source-alist '(markdown-inline "https://github.com/tree-sitter-grammars/tree-sitter-markdown" "split_parser" "tree-sitter-markdown-inline/src")))

;;; ediff
(use-package ediff
  :ensure nil
  :custom
  (ediff-window-setup-function 'ediff-setup-windows-plain)
  (ediff-split-window-function 'split-window-horizontally)
  (ediff-keep-variants nil))

;;; exec-path-from-shell
(when (eq system-type 'gnu/linux)
  (use-package exec-path-from-shell
    :ensure t
    :config
    (when (or (eq window-system 'pgtk) (daemonp))
      (exec-path-from-shell-initialize))
    :custom
    ((exec-path-from-shell-shell-name "fish")
     (exec-path-from-shell-variables '("PATH")))))

;;; vterm
(when (eq system-type 'gnu/linux)
  (use-package vterm
    :ensure t
    :custom
    (vterm-shell "fish")))

;;; coq
(add-hook 'coq-mode-hook #'company-coq-mode)
;; '(coo-compile-before-require t)

;;; vundu
(use-package vundo
  :ensure t)

;; You may want to try smartparens-strict-mode. This enforces that pairs are always balanced, so commands like kill-line keep your code well-formed.
;; coś nie działa nowa wersja
;; smartparens-clojure
;;; smartparens
(use-package smartparens
  :ensure nil
  :hook (prog-mode text-mode markdown-mode)
  :config
  (require 'smartparens-config)
  :bind (:map smartparens-mode-map
              ("C-)" . sp-forward-slurp-sexp)
              ("C-(" . sp-backward-slurp-sexp)
              ("C-}" . sp-forward-barf-sexp)
              ("C-{" . sp-backward-barf-sexp)))

;; ;;; kmacro TODO kmacro-menu-mode oraz list-keyboard-macros
;; (use-package kmacro
;;   :ensure nil
;;   :demand t
;;   :config
;;   (defalias 'kmacro-insert-macro 'insert-kbd-macro)
;;   (defalias 'kmacro-query-macro 'kbd-macro-query)
;;   :bind (:map kmacro-keymap
;;               ("i" . 'kmacro-insert-macro)
;;               ("q" . 'kmacro-query-macro)))

(defalias 'kmacro-insert-macro 'insert-kbd-macro)
(defalias 'kmacro-query-macro 'kbd-macro-query)

(keymap-set kmacro-keymap "i" 'kmacro-insert-macro)
(keymap-set kmacro-keymap "q" 'kmacro-query-macro)

(defalias 'ow-shell
  (kmacro "SPC m ' M-w * SPC x 1 SPC m x s h e l l <return>"))

;; (defalias 'ow-haskell-save-goto-repl
;;   (kmacro "SPC x s SPC c b <escape> i : r <return> <escape>"))

;;; elmacro
;; (use-package elmacro
;;   :ensure t)

;;; racket
;; (use-package racket-mode)

;;; org
(use-package org
  :ensure nil
  :custom
  (org-todo-keywords
   '((sequence "HOLD" "TODO" "DONE" )))
  (org-startup-folded "fold"))

;;; org-chef
;; (use-package org-chef
;;   :ensure t
;;   :custom
;;   (org-capture-templates
;;    '(("c" "Cookbook" entry (file "~/org/cookbook.org")
;;       "%(org-chef-get-recipe-from-url)"
;;       :empty-lines 1)
;;      ("m" "Manual Cookbook" entry (file "~/org/cookbook.org")
;;       "* %^{Recipe title: }\n  :PROPERTIES:\n  :source-url:\n  :servings:\n  :prep-time:\n  :cook-time:\n  :ready-in:\n  :END:\n** Ingredients\n   %?\n** Directions\n\n"))))

;;; dirvish
(use-package dirvish
  :ensure t
  :config
  (dirvish-override-dired-mode)
  :custom
  (dired-listing-switches "-AXBhl --group-directories-first"))

;;; magit
(use-package magit
  :ensure t)

;;; transient
(use-package transient
  :ensure nil
  :bind (:map transient-map
              ("<escape>" . 'transient-quit-one)))

;; (keymap-global-set "M-g" #'casual-avy-tmenu)
;; (keymap-global-set "C-o" #'casual-editkit-main-tmenu)

;; ;;; winner
;; (use-package winner
;;   :ensure nil
;;   :config
;;   (winner-mode t)
;;   :custom
;;   (winner-boring-buffers '("*Completions*" "*which-key*")))

;;; windmove
;; (use-package windmove
;;   :ensure nil
;;   :config
;;   (windmove-mode t)
;;   (windmove-default-keybindings))

;;; emacs
(use-package emacs
  :ensure nil
  :demand t
  :config
  ;; (add-hook 'text-mode-hook 'abbrev-mode) ; try-expand-all-abbrevs
  (put 'narrow-to-region 'disabled nil)
  (put 'suspend-frame 'disabled t)
  (keymap-unset pixel-scroll-precision-mode-map "<next>")
  (keymap-unset pixel-scroll-precision-mode-map "<prior>")
  (global-auto-revert-mode)
  (when (display-graphic-p)
    (context-menu-mode))
  :custom
  (uniquify-buffer-name-style 'forward)
  (auto-revert-avoid-polling t)
  (auto-revert-check-vc-info t)
  (auto-revert-interval 5)
  ;; (backup-directory-alist '(("." . "~/.local/share/Trash/files")))
  (cursor-type 'bar)
  (delete-by-moving-to-trash t)
  (delete-selection-mode t)
  (display-time-default-load-average nil)
  ;; (enable-recursive-minibuffers t) ; źle działa z meow
  (global-page-break-lines-mode)
  (global-visual-line-mode t)
  ;; (global-visual-wrap-prefix-mode t)
  (indent-tabs-mode nil)
  (inhibit-startup-screen t)
  (kmacro-execute-before-append nil)
  (menu-bar-mode nil)
  ;; (package-install-upgrade-built-in t) ; coś smartparens się psuje
  (package-install-upgrade-built-in nil)
  (pixel-scroll-precision-mode t)
  (read-extended-command-predicate #'command-completion-default-include-p)
  ;; (recentf-mode t)
  (ring-bell-function 'ignore)
  (save-place-mode t)
  (scroll-bar-mode nil)
  (sentence-end-double-space nil)
  (set-mark-command-repeat-pop t)
  (tab-always-indent 'complete)
  ;; Emacs 30 and newer: Disable Ispell completion function. As an alternative,
  ;; try `cape-dict'.
  (text-mode-ispell-word-completion nil)
  (tool-bar-mode nil)
  (use-short-answers t)
  (vc-follow-symlinks t)
  :bind (:map global-map ; dodać back do help-map (czyli l w info)
              ("C-x C-b" . 'ibuffer)
              ("<next>" . 'View-scroll-half-page-forward-recenter)
              ("<prior>" . 'View-scroll-half-page-backward-recenter)
              ("C-x C-0" . 'delete-window)
              ;; ("M-<return>" . 'hippie-expand) ; apropos-function ^try-
              ("M-/" . 'hippie-expand) ; apropos-function ^try-
              ;; ("C-x C-t") . 'tab-prefix-map) ; jak zmienić keymapę
              ("<f2>" . 'deadgrep)
              ("M-s a" . 'replace-regexp-as-diff) ; może potem inny keybind
              ;; :map minibuffer-local-map
              ;; ("C-<up>" . previous-history-element) ; <next> i <prior> już są
              ;; ("C-<down>" . next-history-element)
              ))
;; (keymap-substitute global-map "C-x t" "C-x C-t")

;;; corfu
(use-package corfu
  :ensure t
  :init
  (global-corfu-mode)
  :config
  (corfu-history-mode t)
  (corfu-popupinfo-mode t)
  :custom
  (corfu-cycle t)
  :bind (:map corfu-map
              (("SPC" . corfu-insert-separator)
               ("<escape>" . corfu-quit))))

;;; cape TODO po meow dodać cape
(use-package cape
  ;; Bind prefix keymap providing all Cape commands under a mnemonic key.
  ;; Press C-c p ? to for help.
  ;; :bind ("C-c p" . cape-prefix-map) ;; Alternative keys: M-p, M-+, ...
  ;; Alternatively bind Cape commands individually.
  ;; :bind (("C-c p d" . cape-dabbrev)
  ;;        ("C-c p h" . cape-history)
  ;;        ("C-c p f" . cape-file)
  ;;        ...)
  :init
  ;; Add to the global default value of `completion-at-point-functions' which is
  ;; used by `completion-at-point'.  The order of the functions matters, the
  ;; first function returning a result wins.  Note that the list of buffer-local
  ;; completion functions takes precedence over the global list.
  (add-hook 'completion-at-point-functions #'cape-dabbrev)
  (add-hook 'completion-at-point-functions #'cape-file)
  (add-hook 'completion-at-point-functions #'cape-elisp-block)
  (add-hook 'completion-at-point-functions #'cape-history)
  ;; ...
  )
;; (advice-add 'eglot-completion-at-point :around #'cape-wrap-buster)

;;; vertico
(use-package vertico
  :ensure t
  :custom
  (vertico-cycle t)
  (vertico-resize nil)
  :config
  (vertico-mode))

;;; savehist
(use-package savehist
  ;; dodać logikę, żeby tylko gdy jest corfu to dodawać do corfu-history
  :config
  (savehist-mode t)
  :custom
  (savehist-additional-variables '(corfu-history register-alist kill-ring)))

;;; orderless
(use-package orderless
  :ensure t
  :custom
  (completion-styles '(orderless basic))
  ;; (completion-category-defaults nil)
  ;; (completion-category-overrides '((file (styles partial-completion)))) ; cos ze sly źle działa
  )

;;; haskell-mode
(use-package haskell-mode
  :ensure t
  :bind (:map haskell-mode-map
              ("<tab>" . 'completion-at-point)
              ("M-<tab>" . 'indent-for-tab-command)
              ;; ("C-c C-e" . 'ow-haskell-save-goto-repl)
              ;; ("C-c C-b" . 'ow-haskell-save-goto-repl)
              ))

;;; haskell-ts-map
;; (use-package haskell-ts-mode
;;   :ensure t
;;   :config
;;   (with-eval-after-load 'eglot (haskell-ts-setup-eglot)))

(defalias 'ow-haskell-start
  (kmacro "SPC x SPC 1 SPC m x d i r v i s h RET SPC m x d i r v i s h - c o p y - f i l e - n a m e RET q C-c C-b C-x 2 M-x s h r i n k - w i n d o w RET ' ' ' ' ' ' S-<down> C-x 2 S-<left> [ b SPC f SPC m x e l d o c - d o c - b u f f e r RET S-<right> SPC b C-y RET S-<down> SPC b e l d o RET S-<down> SPC b RET C-c C-k S-<left>"))

;;; eglot
(use-package eglot
  :ensure t
  :hook (haskell-mode . eglot-ensure)
  :config
  (setq-default eglot-workspace-configuration
                '(:haskell (:plugin (:stan (:globalOn t))
                                    :formattingProvider "ormolu")
                           :pylsp (:plugins (:ruff (:enabled "true" :formatEnabled "true")))
                           ))
  :custom
  (eglot-autoshutdown t)  ;; shutdown language server after closing last file
  (eglot-confirm-server-initiated-edits nil)  ;; allow edits without confirmation
  )

;; {
;;   "pylsp": {
;;     "plugins": {
;;       "ruff": {
;;         "enabled": true,
;;         "formatEnabled": true,
;;         "executable": "<path-to-ruff-bin>",
;;         "config": "<path_to_custom_ruff_toml>",
;;         "extendSelect": [ "I" ],
;;         "extendIgnore": [ "C90"],
;;         "format": [ "I" ],
;;         "severities": {
;;           "D212": "I"
;;         },
;;         "unsafeFixes": false,
;;         "lineLength": 88,
;;         "exclude": ["__about__.py"],
;;         "select": ["F"],
;;         "ignore": ["D210"],
;;         "perFileIgnores": {
;;           "__init__.py": "CPY001"
;;         },
;;         "preview": false,
;;         "targetVersion": "py310"
;;       }
;;     }
;;   }
;; }

;; '(eglot-confirm-server-edits nil nil nil "Customized with use-package eglot") z customized

;; :bind (:map eglot-mode-map
;;             ("C-c d" . eldoc)
;;             ("C-c a" . eglot-code-actions)
;;             ("C-c f" . flymake-show-buffer-diagnostics)
;;             ("C-c r" . eglot-rename)))

;;; keyfreq
(use-package keyfreq
  :ensure t
  :config
  (keyfreq-mode t)
  (keyfreq-autosave-mode t)
  :custom
  (keyfreq-file "~/.emacs.d/keyfreq"))

;; ;;; keymapy, zrobić z tego komendę
;; ;; (let ((keymap (lookup-key (current-global-map) (kbd "M-s")))
;; (let ((keymap (lookup-key (current-global-map) (kbd "C-x t")))
;;       vars)
;;   (mapatoms (lambda (sym)
;;               (and (boundp sym)
;;                    (eq (symbol-value sym) keymap)
;;                    (push sym vars))))
;;   vars)

;; (defun keymap-symbol (keymap)
;;   "Return the symbol to which KEYMAP is bound, or nil if no such symbol exists."
;;   (catch 'gotit
;;     (mapatoms (lambda (sym)
;;                 (and (boundp sym)
;;                      (eq (symbol-value sym) keymap)
;;                      (not (eq sym 'keymap))
;;                      (throw 'gotit sym))))))


;; ;; in *scratch*:
;; (keymap-symbol (current-local-map))

;;; consult TODO register-use-preview
(use-package consult
  :ensure t
  :init
  (setq register-preview-delay 0.5
        register-preview-function #'consult-register-format)
  ;; Optionally tweak the register preview window.
  ;; This adds thin lines, sorting and hides the mode line of the window.
  (advice-add #'register-preview :override #'consult-register-window)
  ;; Use Consult to select xref locations with preview
  (setq xref-show-xrefs-function #'consult-xref
        xref-show-definitions-function #'consult-xref)
  :config
  (keymap-global-unset "M-g TAB") ; TODO może ładniej
  :custom
  (consult-narrow-key "<")
  :bind (;; C-c bindings in `mode-specific-map'
         ("C-c M-x" . consult-mode-command)
         ("C-c y" . consult-history)
         ("C-c p" . consult-kmacro) ; zobaczyć jeszcze cape-history
         ("C-c n" . consult-man)
         ("C-c i" . consult-info)
         ([remap Info-search] . consult-info)
         ;; C-x bindings in `ctl-x-map'
         ("C-x M-:" . consult-complex-command)     ;; orig. repeat-complex-command
         ("C-x b" . consult-buffer)                ;; orig. switch-to-buffer
         ("C-x 4 b" . consult-buffer-other-window) ;; orig. switch-to-buffer-other-window
         ("C-x 5 b" . consult-buffer-other-frame)  ;; orig. switch-to-buffer-other-frame
         ("C-x t b" . consult-buffer-other-tab)    ;; orig. switch-to-buffer-other-tab
         ("C-x r b" . consult-bookmark)            ;; orig. bookmark-jump
         ("C-x p b" . consult-project-buffer)      ;; orig. project-switch-to-buffer
         ;; Custom M-# bindings for fast register access
         ("M-#" . consult-register-load)
         ("M-'" . consult-register-store)          ;; orig. abbrev-prefix-mark (unrelated)
         ("C-M-#" . consult-register)
         ;; Other custom bindings
         ("M-y" . consult-yank-pop)                ;; orig. yank-pop
         ;; M-g bindings in `goto-map'
         ("M-g e" . consult-compile-error)
         ("M-g f" . consult-flymake)               ;; Alternative: consult-flycheck
         ("M-g g" . consult-goto-line)             ;; orig. goto-line
         ("M-g M-g" . consult-goto-line)           ;; orig. goto-line
         ("M-g o" . consult-outline)               ;; Alternative: consult-org-heading
         ("M-g k" . consult-mark)
         ("M-g K" . consult-global-mark)
         ("M-i" . consult-imenu)
         ("M-g I" . consult-imenu-multi)
         ;; M-s bindings in `search-map'
         ("M-s d" . consult-fd)                  ;; Alternative: consult-fd
         ("M-s c" . consult-locate)
         ("M-s g" . consult-grep)
         ("M-s G" . consult-git-grep)
         ("M-s r" . consult-ripgrep)
         ("M-s l" . consult-line)
         ("M-s L" . consult-line-multi)
         ("M-s k" . consult-keep-lines)
         ("M-s u" . consult-focus-lines)
         ;; Isearch integration
         ("M-s e" . consult-isearch-history)
         :map isearch-mode-map
         ("M-e" . consult-isearch-history)         ;; orig. isearch-edit-string
         ("M-s e" . consult-isearch-history)       ;; orig. isearch-edit-string
         ("M-s l" . consult-line)                  ;; needed by consult-line to detect isearch
         ("M-s L" . consult-line-multi)            ;; needed by consult-line to detect isearch
         ;; Minibuffer history
         :map minibuffer-local-map
         ("M-s" . consult-history)                 ;; orig. next-matching-history-element
         ("M-r" . consult-history))                ;; orig. previous-matching-history-element
  )

;;; marginalia
(use-package marginalia
  :ensure t
  :bind (:map minibuffer-local-map
              ("M-a" . marginalia-cycle))
  :init
  (marginalia-mode))

;;; emabark po klawiaturze
(use-package embark
  :ensure t
  :bind
  (("C-." . embark-act)         ;; pick some comfortable binding
   ("C-;" . embark-dwim)        ;; good alternative: M-.
   ("C-h B" . embark-bindings)) ;; alternative for `describe-bindings'
  )

(use-package embark-consult
  :ensure t
  :hook (embark-collect-mode . consult-preview-at-point-mode))

;;; modus operandi
(use-package modus-themes
  :ensure t
  :demand t
  ;; :init
  ;; (load-theme 'modus-operandi :no-confirm)
  :config (modus-themes-load-theme 'modus-operandi)
  :custom
  (modus-themes-custom-auto-reload nil)
  (modus-themes-italic-constructs t)
  (modus-themes-bold-constructs nil))

;;; font
(set-face-attribute 'default nil
                    :family "noto sans mono"
                    :height 120
                    :weight 'normal
                    :width  'normal)

;;; spacious-padding
(use-package spacious-padding
  :ensure t
  :if (display-graphic-p)
  ;; :hook after-init
  :hook (after-init . spacious-padding-mode)
  :custom
  (spacious-padding-widths
   '( :internal-border-width 15
      :header-line-width 4
      :mode-line-width 6
      :tab-width 4
      :right-divider-width 1
      :scroll-bar-width 8
      :fringe-width 8)))

;;; modeline
(use-package doom-modeline
  :ensure t
  :config (doom-modeline-mode t)
  :custom (doom-modeline-minor-modes nil))

;;; agda
(when (executable-find "agda")
  (load-file (let ((coding-system-for-read 'utf-8))
               (shell-command-to-string "agda-mode locate")))
                                        ;(keymap-set agda2-mode-map "C-c C-i" 'agda2-give)
  )

(defalias 'ow-agda-comment-narrow
  (kmacro "i C-s ` ` ` a g d a RET j x i <escape> f ` J K H H ? C-r ` ` ` a g d a RET k [ b SPC x SPC n n a <escape>"))
(setq safe-local-variable-values '((eval turn-off-auto-fill)))

;;; better movement
(require 'view)

(defun View-scroll-half-page-forward-recenter ()
  "Run `View-scroll-half-page-forward' and `move-to-window-line' in sequence."
  (interactive)
  (View-scroll-half-page-forward)
  (move-to-window-line nil))

(defun View-scroll-half-page-backward-recenter ()
  "Run `View-scroll-half-page-backward' and `move-to-window-line' in sequence."
  (interactive)
  (View-scroll-half-page-backward)
  (move-to-window-line nil))

(keymap-global-set "C-<prior>" 'previous-buffer)
(keymap-global-set "C-<next>" 'next-buffer)

;;; company
(use-package company
  :ensure t)

;;; slime
;; '(lisp-mode-hook '(sly-editing-mode))
;; (defalias 'slime-quick-eval
;;    (kmacro ", d spc c r"))
;; (use-package slime
;;   :ensure t
;;   ;; :custom (inferior-lisp-program "clisp")
;;   :bind ("C-c C-f" . 'slime-quick-eval))
;; (load (expand-file-name "~/.quicklisp/slime-helper.el"))
;; (setq inferior-lisp-program "clisp")
(setq inferior-lisp-program "sbcl")
;; (keymap-set slime-mode-map "C-c C-f" 'slime-quick-eval)

;;; sly, stickery ogarnac
(use-package sly
  :ensure t)
;;   :commands (sly)
;;   ;; :config (setq inferior-lisp-program "sbcl")
;;   :custom (inferior-lisp-program "clisp"))

(use-package all-the-icons
  :ensure t
  :if (display-graphic-p))

;; TODO python-ts-mode -> pyenv -> eglot
;;; python
(use-package python
  :bind (:map python-mode-map ("<tab>" . 'completion-at-point)))

;;; comint
;; (require 'comint)
;; (defun my/comint-set-history-file (file)
;; "load the given history file into comint and write on process exit."
;;   (setq comint-input-ring-file-name file)
;;   (comint-read-input-ring t)
;;   (set-process-sentinel (get-buffer-process (current-buffer))
;;                         #'shell-write-history-on-exity))

;; (defun my/set-python-repl-history-file ()
;;   (my/comint-set-history-file (expand-file-name ".python_history" "~")))
;; (add-hook 'inferior-python-mode-hook #'my/set-python-repl-history-file)

;;; deadgrep
(use-package deadgrep
  :ensure t)

;;; wgrep
(use-package wgrep
  :ensure t
  :custom
  (wgrep-auto-save-buffer t)
  (wgrep-change-readonly-file t))

(use-package wgrep-deadgrep
  :ensure t)

;; (defun deadgrep--include-args (rg-args)
;;   (push "--hidden" rg-args) ;; consider hidden folders/files
;;   (push "--follow" rg-args) ;; follow symlink
;;   )
;; (advice-add 'deadgrep--arguments :filter-return #'deadgrep--include-args)

;; (defvar-keymap consult-keymap
;;   "k" #'consult-kmacro
;;   "i" #'consult-info)

;; (keymap-global-set "C-c a" consult-keymap)

;;; avy
(use-package avy
  :ensure t
  :custom
  (aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l)))

;;; meow
;; TODO
;; beacon grab
(setq meow-paren-keymap (make-keymap))
(meow-define-state paren
  "meow state for interacting with smartparens"
  :lighter " [P]"
  :keymap meow-paren-keymap)

;; meow-define-state creates the variable
(setq meow-cursor-type-paren 'hollow)

(meow-define-keys 'paren
  '("<escape>" . meow-normal-mode)
  '("l" . sp-forward-sexp)
  '("h" . sp-backward-sexp)
  '("j" . sp-down-sexp)
  '("k" . sp-up-sexp)
  '("n" . sp-forward-slurp-sexp)
  '("b" . sp-forward-barf-sexp)
  '("v" . sp-backward-barf-sexp)
  '("c" . sp-backward-slurp-sexp)
  '("u" . meow-undo))

(defun meow-setup ()
  (setq meow-cheatsheet-layout meow-cheatsheet-layout-qwerty)

  (meow-motion-overwrite-define-key
   '("j" . meow-next)
   '("k" . meow-prev)
   '("/" . consult-line))
  (setq meow-selection-command-fallback
        '((meow-change . meow-change-char)
          ;; (meow-kill . meow-c-k)
          (meow-kill . meow-delete)
          (meow-cancel-selection . keyboard-quit) ; TODO zobaczyć czy fajne
          ;; (meow-cancel-selection . ignore)
          (meow-pop-selection . meow-pop-grab)
          (meow-beacon-change . meow-beacon-change-char)))
  (meow-leader-define-key
   ;; spc j/k will run the original command in motion state.
   '("/" . "H-/")
   '("j" . "H-j")
   '("b" . consult-buffer)
   '("q" . delete-window)
   '("k" . kill-current-buffer)
   '("s" . split-window-right)
   '("w" . delete-other-windows)
   '("u" . universal-argument)
   '("o" . ace-window)
   ;; '("o" . other-window)
   '("t" . "C-x t")
   ;; '("k" . "H-k")
   ;; '("i" . consult-imenu) ; mam spc m i
   ;; '("u" . meow-universal-argument) ; może po prostu C-u?
   ;; '("y" . ("yas" . yas-keymap)) ; TODO po co to jest https://github.com/meow-edit/meow/issues/537
   ;; use spc (0-9) for digit arguments.
   '("1" . meow-digit-argument)
   '("2" . meow-digit-argument)
   '("3" . meow-digit-argument)
   '("4" . meow-digit-argument)
   '("5" . meow-digit-argument)
   '("6" . meow-digit-argument)
   '("7" . meow-digit-argument)
   '("8" . meow-digit-argument)
   '("9" . meow-digit-argument)
   '("0" . meow-digit-argument)
   '("/" . meow-keypad-describe-key)
   '("?" . meow-cheatsheet))
  (meow-normal-define-key
   '("0" . meow-expand-0)
   '("9" . meow-expand-9)
   '("8" . meow-expand-8)
   '("7" . meow-expand-7)
   '("6" . meow-expand-6)
   '("5" . meow-expand-5)
   '("4" . meow-expand-4)
   '("3" . meow-expand-3)
   '("2" . meow-expand-2)
   '("1" . meow-expand-1)
   '(")" . meow-forward-slurp)
   '("(" . meow-forward-barf)
   '("{" . meow-backward-slurp)
   '("}" . meow-backward-barf)
   '("-" . negative-argument)
   '(";" . meow-reverse)
   '("," . meow-inner-of-thing)
   '("." . meow-bounds-of-thing)
   '("[" . meow-beginning-of-thing)
   '("]" . meow-end-of-thing)
   '("a" . meow-append)
   '("b" . meow-back-word)
   '("B" . backward-word)
   ;; '("B" . meow-back-symbol)
   '("c" . meow-change)
   ;; '("d" . meow-delete)
   '("d" . meow-kill)
   ;; '("D" . meow-backward-delete)
   '("e" . meow-next-word)
   '("E" . forward-word)
   ;; '("E" . meow-next-symbol)
   '("f" . meow-find)
   ;; '("F" . isearch-forward)
   '("g" . meow-join)
   ;; '("G" . meow-grab)
   '("h" . meow-left)
   '("H" . meow-left-expand)
   '("i" . meow-insert)
   '(":a" . meow-left) ; why does this works?
   '(":b" . meow-right)
   ;; '("I" . meow-open-above)
   '("j" . meow-next)
   '("J" . meow-next-expand)
   '("k" . meow-prev)
   '("K" . meow-prev-expand)
   '("l" . meow-right)
   '("L" . meow-right-expand)
   '("m" . avy-goto-char-timer)
   '("n" . meow-search)
   '("o" . meow-open-below)
   '("O" . meow-open-above)
   ;; '("o" . meow-block)
   ;; '("O" . meow-to-block)
   '("p" . meow-yank)
   ;; (cons "P" consult-keymap)
   '("q" . meow-quit)
   '("Q" . meow-goto-line)
   '("r" . meow-replace)
   '("R" . meow-swap-grab)
   '("s" . execute-extended-command)
   '("S" . meow-grab)
   ;; '("s" . meow-kill)
   '("t" . meow-till)
   '("u" . meow-undo)
   '("U" . undo-redo)
   ;;'("U" . meow-undo-in-selection)
   ;; '("v" . meow-visit)
   '("v" . meow-block)
   '("V" . meow-to-block)
   '("/" . consult-line)
   '("?" . comment-dwim)
   '("w" . meow-mark-word)
   '("W" . meow-mark-symbol)
   '("x" . meow-line)
   '("X" . meow-line-expand)
   '("y" . meow-save)
   '("Y" . meow-sync-grab)
   '("z" . meow-pop-selection)
   '("Z" . meow-paren-mode)
   '("'" . repeat)
   '("<escape>" . meow-cancel-selection)
   '("%" . meow-query-replace)
   '("&" . meow-query-replace-regexp)
   ))

;; ;; '("/" . consult-line) ; todo zmapować consult-line w emacsie i dodać tutaj skrót klawiszowy
;; ;; '("?" . comment-dwim) ; todo zmapować comment-dwim w emacsie i dodać tutaj skrót klawiszowy

;; TODO Add meow-pop-to-mark and meow-unpop-to-mark
(use-package meow
  :ensure t
  :config
  (meow-setup)
  (meow-global-mode 1)
  (add-to-list 'meow-mode-state-list '(ediff-mode . insert)) ; nwm czemu nie działa
  :custom
  (meow-expand-exclude-mode-list '())
  (meow-esc-delay 0.001)
  (meow-select-on-insert nil)
  (meow-select-on-append nil)
  )

(meow-thing-register 'latex
                     '(regexp "\\$" "\\$")
                     '(regexp "\\$" "\\$"))

(add-to-list 'meow-char-thing-table
             (cons ?x 'latex))

(meow-thing-register 'quoted
                     '(regexp "`" "`\\|'")
                     '(regexp "`" "`\\|'"))

(add-to-list 'meow-char-thing-table
             '(?' . quoted))


;; (use-package meow-tree-sitter
;;   :ensure t
;;   :config
;;   (meow-tree-sitter-register-defaults))

(use-package which-key
  :ensure nil
  :config
  (which-key-mode 1)
  :custom
  (which-key-side-window-location 'bottom)
  (which-key-sort-order #'which-key-key-order-alpha)
  (which-key-allow-imprecise-window-fit nil)
  (which-key-sort-uppercase-first nil)
  (which-key-add-column-padding 1)
  (which-key-max-display-columns nil)
  (which-key-min-display-lines 6)
  (which-key-side-window-slot -10)
  (which-key-side-window-max-height 0.25)
  (which-key-idle-delay 0.2)
  ;; which-key-max-description-length 25)
  (which-key-max-description-length 50)
  (which-key-allow-imprecise-window-fit nil)
  (which-key-separator " > "))

;;; custom
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("b29ba9bfdb34d71ecf3322951425a73d825fb2c002434282d2e0e8c44fce8185"
     default))
 '(eglot-confirm-server-edits nil nil nil "Customized with use-package eglot")
 '(eldoc-echo-area-prefer-doc-buffer t)
 '(package-selected-packages
   '(ace-window all-the-icons cape casual-suite cider clojure-mode
                colorful-mode company corfu deadgrep dirvish
                doom-modeline editorconfig eldoc-box embark-consult
                ement exec-path-from-shell faceup fireplace flutter
                go-translate gptel haskell-mode haskell-ts-mode
                idlwave inf-clojure keyfreq marginalia markdown-mode
                markdown-ts-mode meow meow-tree-sitter modus-themes
                multiple-cursors orderless pdf-tools pyenv-mode pyvenv
                sly smartparens spacious-padding sweeprolog
                tempel-collection use-package verilog-mode vertico
                visible-mark vterm vundo wgrep-deadgrep which-key
                yasnippet-snippets))
 '(scroll-preserve-screen-position 1))

;; (custom-set-faces
;;  ;; custom-set-faces was added by Custom.
;;  ;; If you edit it by hand, you could mess it up, so be careful.
;;  ;; Your init file should contain only one such instance.
;;  ;; If there is more than one, they won't work right.
;;  '(fringe ((t :background "#ffffff")))
;;  '(header-line ((t :box (:line-width 4 :color "#f2f2f2" :style nil))))
;;  '(header-line-highlight ((t :box (:color "#000000"))))
;;  '(keycast-key ((t)))
;;  '(line-number ((t :background "#ffffff")))
;;  '(mode-line ((t :box (:line-width 6 :color "#c8c8c8" :style nil))))
;;  '(mode-line-active ((t :box (:line-width 6 :color "#c8c8c8" :style nil))))
;;  '(mode-line-highlight ((t :box (:color "#000000"))))
;;  '(mode-line-inactive ((t :box (:line-width 6 :color "#e6e6e6" :style nil))))
;;  '(tab-bar-tab ((t :box (:line-width 4 :color "#ffffff" :style nil))))
;;  '(tab-bar-tab-inactive ((t :box (:line-width 4 :color "#c2c2c2" :style nil))))
;;  '(tab-line-tab ((t)))
;;  '(tab-line-tab-active ((t)))
;;  '(tab-line-tab-inactive ((t)))
;;  '(vertical-border ((t :background "#ffffff" :foreground "#ffffff")))
;;  '(window-divider ((t nil)))
;;  '(window-divider-first-pixel ((t nil)))
;;  '(window-divider-last-pixel ((t nil))))
(put 'dired-find-alternate-file 'disabled nil)
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(fringe ((t :background "#ffffff")))
 '(header-line ((t :box (:line-width 4 :color "#f2f2f2" :style nil))))
 '(header-line-highlight ((t :box (:color "#000000"))))
 '(keycast-key ((t)))
 '(line-number ((t :background "#ffffff")))
 '(mode-line ((t :box (:line-width 6 :color "#c8c8c8" :style nil))))
 '(mode-line-active ((t :box (:line-width 6 :color "#c8c8c8" :style nil))))
 '(mode-line-highlight ((t :box (:color "#000000"))))
 '(mode-line-inactive ((t :box (:line-width 6 :color "#e6e6e6" :style nil))))
 '(tab-bar-tab ((t :box (:line-width 4 :color "#ffffff" :style nil))))
 '(tab-bar-tab-inactive ((t :box (:line-width 4 :color "#c2c2c2" :style nil))))
 '(tab-line-tab ((t)))
 '(tab-line-tab-active ((t)))
 '(tab-line-tab-inactive ((t)))
 '(vertical-border ((t :background "#ffffff" :foreground "#ffffff")))
 '(window-divider ((t nil)))
 '(window-divider-first-pixel ((t nil)))
 '(window-divider-last-pixel ((t nil))))
(put 'downcase-region 'disabled nil)
