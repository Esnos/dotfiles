;; zero alta, tylko ctrl, bo mam go lepiej ustawionego

(keymap-set global-map "C-f" 'isearch-forward)
(keymap-set global-map "<escape>" 'keyboard-quit)

(defun prot/keyboard-quit-dwim ()
  "Do-What-I-Mean behaviour for a general `keyboard-quit'.

The generic `keyboard-quit' does not do the expected thing when
the minibuffer is open.  Whereas we want it to close the
minibuffer, even without explicitly focusing it.

The DWIM behaviour of this command is as follows:

- When the region is active, disable it.
- When a minibuffer is open, but not focused, close the minibuffer.
- When the Completions buffer is selected, close it.
- In every other case use the regular `keyboard-quit'."
  (interactive)
  (cond
   ((region-active-p)
    (keyboard-quit))
   ((derived-mode-p 'completion-list-mode)
    (delete-completion-window))
   ((> (minibuffer-depth) 0)
    (abort-recursive-edit))
   (t
    (keyboard-quit))))

(keymap-set global-map "<escape>" 'prot/keyboard-quit-dwim)
(keymap-set global-map "C-v" 'yank)
(keymap-set global-map "C-M-v" 'consult-yank-pop)
(keymap-set global-map "C-z" 'undo)
(keymap-set global-map "C-S-z" 'undo-redo)
(keymap-set global-map "C-/" 'comment-dwim)

(keymap-set global-map "S-<return>" 'execute-extended-command)
(setq mark-ring-max 6)
