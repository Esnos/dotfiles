(defun my-locate-library ()
  "Open located library in buffer"
  (interactive)
  (call-interactively #'locate-library)
  (let ((echo-file (current-message)))
    (string-match "Library is file \\(.*\\)"
                  echo-file)
    (find-file (file-name-with-extension (match-string 1 echo-file)
                                         "el"))))

;; (defun my-query-replace ()
;;   "Yank selected region to `query-replace' if mark is before point"
;;   (interactive)
;;   (when (not (and (region-active-p)
;;                   (< (mark)
;;                      (point))))
;;     (call-interactively #'query-replace))
;;   (let ((phrase (buffer-substring-no-properties (region-beginning)
;;                                                 (region-end))))
;;     (query-replace (prog1 phrase
;;                      (goto-char (mark))
;;                      (deactivate-mark))
;;                    (read-string (format "Query replace %s with: " phrase)))))

(defun xah-search-current-word ()
  "Call `isearch' on current word or selection.
“word” here is A to Z, a to z, and hyphen [-] and lowline [_], independent of syntax table.

URL `http://xahlee.info/emacs/emacs/modernization_isearch.html'
Version: 2015-04-09"
  (interactive)
  (let (xp1 xp2)
    (if (region-active-p)
        (setq xp1 (region-beginning) xp2 (region-end))
      (save-excursion
        (skip-chars-backward "-_A-Za-z0-9")
        (setq xp1 (point))
        (right-char)
        (skip-chars-forward "-_A-Za-z0-9")
        (setq xp2 (point))))
    (setq mark-active nil)
    (when (< xp1 (point))
      (goto-char xp1))
    (isearch-mode t)
    (isearch-yank-string (buffer-substring-no-properties xp1 xp2))))
(keymap-global-set "<f1>" 'xah-search-current-word)

;; (defun test-message-foo nil
;;   (message "foo"))

;; (defun kill-test-message-foo nil
;;   (kill-new (current-message)))

;; (advice-add 'test-message-foo :after 'kill-test-message-foo)
;; (test-message-foo)

(defun my-copy-comment-paste-region (start end)
  "Copy the selected region, comment it, and paste the copy below."
  (interactive "r")
  (let ((text (buffer-substring-no-properties start end)))
    ;; Copy the region
    (kill-ring-save start end)
    ;; Comment the region
    (comment-region start end)
    ;; Move to the end of the region
    (end-of-line)
    ;; Insert a newline
    (newline)
    ;; Insert the copied text
    (insert text)))

;; Bind the function to a key combination (optional)
;; stokrotka ma crocsy
;; stokrotka ma crocsy
;; stokrotka ma crocsy
;; stokrotka ma crocsy
;; stokrotka ma crocsy
