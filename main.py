import board

from kmk.keys import KC
from storage import getmount
from kmk.kmk_keyboard import KMKKeyboard as _KMKKeyboard
from kmk.scanners.keypad import KeysScanner

from kmk.modules.layers import Layers
from kmk.modules.split import Split, SplitSide
from kmk.modules.sticky_keys import StickyKeys
from kmk.extensions.media_keys import MediaKeys
from kmk.modules.dynamic_sequences import DynamicSequences

name = str(getmount("/").label)
is_right = True if name.endswith("R") else False

_KEY_CFG_LEFT = [
    board.GP1, board.GP2, board.GP5, board.GP4, board.GP3,
    board.GP6, board.GP27, board.GP7, board.GP26, board.GP8,
    board.GP9, board.GP21, board.GP10, board.GP20, board.GP11,
    board.GP19, board.GP12, board.GP18,
]

_KEY_CFG_RIGHT = [
    board.GP3, board.GP4, board.GP5, board.GP2, board.GP1,
    board.GP8, board.GP26, board.GP7, board.GP27, board.GP6,
    board.GP11, board.GP20, board.GP10, board.GP21, board.GP9,
    board.GP18, board.GP12, board.GP19,
]

class KMKKeyboard(_KMKKeyboard):
    def __init__(self):
        self.matrix = KeysScanner(
            pins=_KEY_CFG_RIGHT if is_right else _KEY_CFG_LEFT,
            value_when_pressed=False,
            interval=0.01, 
            debounce_threshold=3,
        )
    # fmt: off
    coord_mapping = [
        0, 1, 2, 3, 4,      18, 19, 20, 21, 22,
        5, 6, 7, 8, 9,      23, 24, 25, 26, 27,
        10, 11, 12, 13, 14,  28, 29, 30, 31, 32, # noqa
        15, 16, 17,  33, 34, 35 # noqa

    ]

keyboard = KMKKeyboard()
keyboard.debug_enabled = True
### moja klawiaturka
# proba custom key
from kmk.keys import Key

class LimitKey(Key):
    def __init__(self, key, limit):
        self.key = key
        self.limit = limit

    def on_press(self, keyboard, coord_int=None):
        if self.limit > 0:
            keyboard.add_key(self.key)
            print("hej, tutuaj", keyboard.keys_pressed)

    def on_release(self, keyboard, coord_int=None):
        if self.limit > 0:
            self.limit -= 1
            keyboard.remove_key(self.key)

KC_A10 = LimitKey(KC.PLUS, 1000)
KC_B20 = LimitKey(KC.B, 20)

def plus_rown():
    print("hej, tutaj serio")
    if keyboard.keys_pressed == "{ModifierKey(code=2), KeyboardKey(code=46)}":
        Press(KC.EQL)
    else:
        Press(KC.COLN)
    print("hej, tutuaj", keyboard.keys_pressed)

# Macros
from kmk.modules.macros import Macros

keyboard.modules.append(Macros())
WOW = KC.MACRO("Wow, KMK is awesome!")

from kmk.modules.macros import Press, Release, Tap

PASTE_WITH_COMMENTARY = KC.MACRO(
    "look at this: ",
    Press(KC.LCTL),
    Tap(KC.Y),
    Release(KC.LCTL)
)

from kmk.modules.macros import Tap, Delay

COUNTDOWN_TO_PASTE = KC.MACRO(
    Tap(KC.N3),
    Tap(KC.ENTER),
    Delay(1000),
    Tap(KC.N2),
    Tap(KC.ENTER),
    Delay(1000),
    Tap(KC.N1),
    Tap(KC.ENTER),
    Delay(1000),
    Tap(KC.LCTL(KC.V)),
)
# Layer
keyboard.modules.append(Layers())

# Media key
keyboard.extensions.append(MediaKeys())

# one-shot
sticky_keys = StickyKeys(release_after=2000)
keyboard.modules.append(sticky_keys)
# dynamic sequence
keyboard.modules.append(DynamicSequences())

# Split
split_side = SplitSide.RIGHT if is_right else SplitSide.LEFT
split = Split(
    split_side=split_side,
    split_target_left=True,
    uart_interval=20,
    data_pin=board.GP17,  # RX
    data_pin2=board.GP16,  # TX
    uart_flip=False,
    use_pio=False,
)
keyboard.modules.append(split)

def ej(x):
    print("lisy so super", keyboard.keys_pressed)
    return x

sticky = lambda x: KC.STICKY(x, defer_release=True, retap_cancel=True)
keyboard.keymap = [
    # layer 0
    [
        KC.Q, KC.W, KC.E, KC.R, KC.T, KC.Y, KC.U, KC.I, KC.O, KC.P,
        KC.A, KC.S, KC.D, KC.F, KC.G, KC.H, KC.J, KC.K, KC.L, KC.ESC,
        KC.Z, KC.X, KC.C, KC.V, KC.B, KC.N, KC.M, KC.COMM, KC.DOT, KC.SLSH,
        KC.LCTRL, KC.MO(1), KC.MO(4), KC.SPC, KC.MO(2), KC.MO(5),
    ],
    # layer 1
    [
        KC.LGUI, KC.LCTL(KC.W), KC.LCTL(KC.D), KC.LCTL(KC.U), KC.LCTL(KC.T), KC.PGUP, KC.HOME, KC.UP, KC.END, KC.TO(0),
        sticky(KC.LALT), sticky(KC.LGUI), sticky(KC.LSFT), sticky(KC.LCTL), KC.LCTL(KC.F), KC.PGDN, KC.LEFT, KC.DOWN, KC.RIGHT, KC.TRNS,
        KC.LCTL(KC.Z), KC.LCTL(KC.X), KC.LCTL(KC.C), KC.LCTL(KC.V), KC.PSCR, KC.SPC, KC.ENT, KC.TAB, KC.DEL, KC.TRNS,
        KC.TRNS, KC.TRNS, KC.TRNS, KC.BSPC, KC.MO(3), KC.TRNS,
    ],
    # layer 2
    [
        KC.N7, KC.N5, KC.N3, KC.N1, KC.N9, KC.N8, KC.N0, KC.N2, KC.N4, KC.N6,
        KC_A10, KC.COMM, KC.LPRN, KC.RPRN, KC.RABK, KC.MINS, KC.EQL, KC.COLN , KC.DQUO, KC.DOT, # czemu coln robi plusa z eql
        WOW, KC.UNDERSCORE, KC.LBRC, KC.RBRC, KC.LABK, KC.SPC, KC.ENT, KC.TAB, KC.BSPC, KC.NO,
        COUNTDOWN_TO_PASTE, KC.MO(3), KC.MO(7), KC.TRNS, KC.TRNS, KC.TRNS,
    ],
    # layer 3
    [
        PASTE_WITH_COMMENTARY, KC.TO(1), KC.NO, KC.TO(8), KC.NO, KC.RECORD_SEQUENCE(), KC.F9, KC.F10, KC.F11, KC.F12,
        KC.INS, KC.MUTE, KC.VOLD, KC.VOLU, KC.MPLY, KC.PLAY_SEQUENCE(), KC.F1, KC.F2, KC.F3, KC.F4,
        KC.NO, KC.LGUI, KC.LSFT, KC.LCTL, KC.RCTL, KC.STOP_SEQUENCE(), KC.F5, KC.F6, KC.F7, KC.F8,
        KC.TRNS, KC.TRNS, KC.TRNS, KC.TRNS, KC.TRNS, KC.TRNS,
    ],
    # layer 4
    [
        KC.LSFT(KC.Q), KC.LSFT(KC.W), KC.LSFT(KC.E), KC.LSFT(KC.R), KC.LSFT(KC.T), KC.LSFT(KC.Y), KC.LSFT(KC.U), KC.LSFT(KC.I), KC.LSFT(KC.O), KC.LSFT(KC.P),
        KC.LSFT(KC.A), KC.LSFT(KC.S), KC.LSFT(KC.D), KC.LSFT(KC.F), KC.LSFT(KC.G), KC.LSFT(KC.H), KC.LSFT(KC.J), KC.LSFT(KC.K), KC.LSFT(KC.L), KC.MO(6),
        KC.LSFT(KC.Z), KC.LSFT(KC.X), KC.LSFT(KC.C), KC.LSFT(KC.V), KC.LSFT(KC.B), KC.LSFT(KC.N), KC.LSFT(KC.M), KC.UNDS, KC.BSPC, KC.QUES,
        KC.TRNS, KC.TRNS, KC.TRNS, KC.TRNS, KC.MO(7), KC.TRNS,
    ],
    # layer 5
    [
        KC.NO, KC.LALT(KC.W), KC.RALT(KC.E), KC.NO, KC.NO, KC.LALT(KC.Y), KC.NO, KC.NO, KC.RALT(KC.O), KC.NO,
        KC.RALT(KC.A), KC.RALT(KC.S), KC.GRAVE, KC.TILDE, KC.NO, KC.NO, KC.LALT(KC.N), KC.LALT(KC.P), KC.RALT(KC.L), KC.TRNS,
        KC.RALT(KC.Z), KC.RALT(KC.X), KC.RALT(KC.C), KC.NO, KC.NO, KC.RALT(KC.N), KC.NO, KC.NO, KC.BSPC, KC.LALT(KC.SLASH),
        KC.TRNS, KC.TRNS, KC.MO(6), KC.TRNS, KC.TRNS, KC.TRNS,
    ],
    # layer 6
    [
        KC.NO, KC.NO, KC.LSFT(KC.RALT(KC.E)), KC.NO, KC.NO, KC.NO, KC.NO, KC.NO, KC.LSFT(KC.RALT(KC.O)), KC.NO,
        KC.LSFT(KC.RALT(KC.A)), KC.LSFT(KC.RALT(KC.S)), KC.NO, KC.NO, KC.NO, KC.NO, KC.NO, KC.NO, KC.LSFT(KC.RALT(KC.L)), KC.TRNS,
        KC.LSFT(KC.RALT(KC.Z)), KC.LSFT(KC.RALT(KC.X)), KC.LSFT(KC.RALT(KC.C)), KC.NO, KC.NO, KC.LSFT(KC.RALT(KC.N)), KC.NO, KC.NO, KC.BSPC, KC.NO,
        KC.TRNS, KC.TRNS, KC.TRNS, KC.TRNS, KC.TRNS, KC.TRNS,
    ],
    # layer 7
    [
        KC.NO, KC.AT, KC.HASH, KC.AMPR, KC.NO, KC.NO, KC.BSLS, KC.PAST, KC.PIPE, KC.NO,
        KC.NO, KC.CIRC, KC.PERC, KC.DLR, KC.NO, KC.NO, KC.PLUS , KC.SCLN, KC.QUOT, KC.NO,
        KC.NO, KC.EXLM, KC.LCBR, KC.RCBR, KC.NO, KC.SPC, KC.ENT, KC.TAB, KC.BSPC, KC.NO,
        KC.TRNS, KC.TRNS, KC.TRNS, KC.TRNS, KC.TRNS, KC.TRNS,
    ],
    # layer 8
    [
        KC.ESC, KC.Q, KC.W, KC.E, KC.R, KC.NO, KC.NO, KC.NO, KC.NO, KC.TO(0),
        KC.LSFT, KC.A, KC.S, KC.D, KC.F, KC.NO, KC.ENT, KC.HOME, KC.NO, KC.NO,
        KC.LCTL, KC.Z, KC.X, KC.C, KC.V, KC.NO, KC.LCTL(KC.C), KC.LCTL(KC.V), KC.NO, KC.NO,
        KC.NO, KC.NO, KC.SPC, KC.NO, KC.NO, KC.NO,
    ],
]

if __name__ == '__main__':
    keyboard.go()
