#!/bin/bash
# Get clipboard content
if command -v wl-paste &> /dev/null; then
    clipboard=$(wl-paste --no-newline)
elif command -v xclip &> /dev/null; then
    clipboard=$(xclip -selection clipboard -o)
else
    clipboard="Clipboard tool not found"
fi

# Output the clipboard content (truncate if too long)
echo -n "${clipboard:0:50}"  # Truncate to 50 characters
