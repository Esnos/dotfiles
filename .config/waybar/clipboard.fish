#!/usr/bin/env fish

# 1. Check if wl-paste or xclip is available and get the clipboard content
if type -q wl-paste
    set clipboard (wl-paste --no-newline)
else if type -q xclip
    set clipboard (xclip -selection clipboard -o)
else
    set clipboard "Clipboard tool not found"
end

# 2. Collapse multiline content into a single line
set clipboard (string join ' ' $clipboard)

# 3. Truncate to 50 characters (adjust as you wish)
set clipboard (string sub -l 50 $clipboard)

# 4. Print the result
echo -n $clipboard

# 5. Wait 3 seconds, then clear
# sleep 3
# echo ""
