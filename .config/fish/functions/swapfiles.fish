function swapfiles --description 'swaps files'
    set -l options s/suffix=
    argparse -n swapfiles $options -- $argv
    or return

    # helper function, swaps two files
    function swap --argument file1 file2
        for filepath in $argv
            if ! test -f $filepath
                echo "File not found: $filepath" >&2 && return 1
            end
        end
        set tempfile $file1.swap
        mv -- $file1 $tempfile && mv -- $file2 $file1 && mv -- $tempfile $file2
    end

    # if ext flag is used, swap every file in argv with file+suffix
    if set -ql _flag_suffix
        for file1 in $argv
            set -l base (path change-extension "" -- $file1)
            set -l ext (path extension "" -- $file1)
            set -l file2 (string join "" -- $base $_flag_suffix $ext)
            swap $file1 $file2
        end
        return 0
    else
        # if ext flag is not used,
        # check if files have same file extension and check if there are two arguments  
        if test (count $argv) -ne 2
            echo "Expecting 2 file arguments" >&2 && return 1
        end

        if test (path extension -- $argv[1]) != (path extension -- $argv[2])
            echo "files should have same file extension" >&2 && return 1
        end

        swap $argv[1] $argv[2]; or return
    end 
end
