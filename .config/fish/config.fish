if status is-interactive
    # Commands to run in interactive sessions can go here
end

function sc
   cd $argv
   ls
end

function vterm_printf;
    if begin; [  -n "$TMUX" ]  ; and  string match -q -r "screen|tmux" "$TERM"; end
        # tell tmux to pass the escape sequences through
        printf "\ePtmux;\e\e]%s\007\e\\" "$argv"
    else if string match -q -- "screen*" "$TERM"
        # GNU screen (screen, screen-256color, screen-256color-bce)
        printf "\eP\e]%s\007\e\\" "$argv"
    else
        printf "\e]%s\e\\" "$argv"
    end
end

if [ "$INSIDE_EMACS" = 'vterm' ]
    function clear
        vterm_printf "51;Evterm-clear-scrollback";
        tput clear;
    end
end

alias hx "helix"
alias ll "eza --all --long --links --color=always --group-directories-first"
alias ls "eza --all --color=always --group-directories-first"
alias tree "eza --all --git-ignore --tree --color=always --group-directories-first --level=3"
alias tp "trash-put"
alias .. 'cd ..'
alias grep "grep --extended-regexp --binary-files=without-match --color=always"
alias sed "sed --regexp-extended"
alias chromium "chromium --ozone-platform-hint=auto"

set -x TERMINAL "alacritty"
set -x EDITOR "helix"
set -x VISUAL "helix"
# set -x EDITOR "emacsclient -t"
# set -x VISUAL "emacsclient --create-frame --alternate-editor="" "
set -x PAGER "most"
set -x XDG_CURRENT_DESKTOP "sway"

bind \e\[3\;5~ kill-word
bind \cH backward-kill-word
# set fish_function_path $fish_function_path "/usr/share/powerline/bindings/fish"

# function fish_greeting
#      pokemon-colorscripts --random-by-names charmander,bulbasaur,squirtle --no-title
# end


# Created by `pipx` on 2023-09-22 09:39:16
# set PATH $PATH /home/esnos/.local/bin

# set -x PATH $PATH ~/.nix-profile/bin

# set -q GHCUP_INSTALL_BASE_PREFIX[1]; or set GHCUP_INSTALL_BASE_PREFIX $HOME ; set -gx PATH $HOME/.cabal/bin /home/esnos/.ghcup/bin $PATH # ghcup-env
