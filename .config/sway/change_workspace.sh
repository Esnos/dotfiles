ws="$(swaymsg -t get_workspaces | jq -r '.[].name' | wofi --show=dmenu)"
if [ -n  "$ws" ]; then
    swaymsg workspace "$ws"
fi

