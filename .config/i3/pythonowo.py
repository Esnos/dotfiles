#!/usr/bin/env python3
import subprocess
import argparse

# Configuration
FG_COLOR = "#bbbbbb"
BG_COLOR = "#111111"
HLFG_COLOR = "#111111"
HLBG_COLOR = "#bbbbbb"
BORDER_COLOR = "#222222"

ROFI_OPTIONS = ["-theme", "~/.config/rofi/powermenu.rasi"]

ZENITY_TITLE = "Power Menu"
ZENITY_TEXT = "Action:"
ZENITY_OPTIONS = ["--column=", "--hide-header"]

enable_confirmation = False
preferred_launcher = "rofi"

# Check whether the user-defined launcher is valid
launcher_list = ["rofi", "zenity"]
if preferred_launcher not in launcher_list:
    print(f"Supported launchers: {', '.join(launcher_list)}")
    exit(1)
else:
    # Get array with unique elements and preferred launcher first
    launcher_list.remove(preferred_launcher)
    launcher_list.insert(0, preferred_launcher)

# Parse CLI arguments

parser = argparse.ArgumentParser(description="Display a menu for shutdown, reboot, lock, etc.")
parser.add_argument("-c", action="store_true", help="Ask for user confirmation")
parser.add_argument("-p", metavar="launcher", choices=launcher_list, default=preferred_launcher,
                    help="Preferred launcher (rofi or zenity)")
args = parser.parse_args()

enable_confirmation = args.c
preferred_launcher = args.p

# Menu entries
menu = {
    " Shutdown": "systemctl poweroff",
    " Reboot": "systemctl reboot",
    " Suspend": "systemctl suspend",
    " Hibernate": "systemctl hibernate",
    # " Lock": "~/.config/i3/scripts/blur-lock",
    " Logout": "i3-msg exit",
    " Cancel": ""
}

menu_nrows = len(menu)

# Menu entries that may trigger a confirmation message
menu_confirm = ["Shutdown", "Reboot", "Hibernate", "Suspend", "Halt", "Logout"]

# Launcher options
rofi_colors = ["-bc", BORDER_COLOR, "-bg", BG_COLOR, "-fg", FG_COLOR,
               "-hlfg", HLFG_COLOR, "-hlbg", HLBG_COLOR]
rofi_options = ["-dmenu", "-i", "-lines", str(menu_nrows), "-p", "Power Menu"]

# Choose launcher
launcher_exe = ""
launcher_options = []

# for launcher in launcher_list:
#     if subprocess.run(["command", "-v", launcher]).returncode == 0:
#         if launcher == "rofi":
            # launcher_exe = "rofi"
            # launcher_options = rofi_options + rofi_colors + ROFI_OPTIONS
#         elif launcher == "zenity":
#             launcher_exe = "zenity"
#             launcher_options = ["--list", "--title", ZENITY_TITLE, "--text", ZENITY_TEXT] + ZENITY_OPTIONS
#         break

# No launcher available
launcher_exe = "rofi"
launcher_options = rofi_options + rofi_colors + ROFI_OPTIONS
if launcher_exe == "":
    exit(1)

# Show menu
selection = subprocess.run([launcher_exe] + launcher_options, capture_output=True, text=True).stdout.strip()

# Ask for confirmation if needed
if selection in menu_confirm and enable_confirmation:
    confirmed = subprocess.run([launcher_exe, "-dmenu", "-i", "-lines", "2", "-p", f"{selection}?",
                                *rofi_colors, *ROFI_OPTIONS],
                               capture_output=True, text=True, input="Yes\nNo").stdout.strip()

    if confirmed == "Yes":
        confirmed = 0
    else:
        confirmed = 1

    if confirmed == 0:
        subprocess.run(["i3-msg", "-q", f"exec --no-startup-id {menu[selection]}"])

elif selection:
    subprocess.run(["i3-msg", "-q", f"exec --no-startup-id {menu[selection]}"])

